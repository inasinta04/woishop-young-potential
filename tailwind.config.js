/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./public/**/*.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        'green': '#03AC0E',
        'grey': '#F5F5F5',
        'dark-grey' : '#7A7A7A',
        'red' : '#F62845'
      }
    },
  },
  plugins: [],
}
