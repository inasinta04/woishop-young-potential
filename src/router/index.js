import { createRouter, createWebHistory } from 'vue-router'

import Login from '../components/Login/Login.vue'
import Register from '../components/Register/Register.vue';
import VerifikasiOTP from '../components/OTP/OTP.vue'
import SuccessLogin from '../components/SuccessLogin/SuccessLogin.vue';

const routes = [
    { path: '/', component: Login },
    { path: '/Login', component: Login },
    { path: '/Register', component: Register},
    { path: '/VerifikasiOTP', component: VerifikasiOTP},
    { path: '/SuccessLogin', component: SuccessLogin}
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
});

export default router;